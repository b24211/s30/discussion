// Aggregation in MongoDB

// Creating fruits documents

db.fruits.insertMany([
    {
        name: "Apple",
        color: "Red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Phillipines", "US"]
    },
    {
        name: "Banana",
        color: "Yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Phillipines", "Ecuador"]
    },
    {
        name: "Kiwi",
        color: "Green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },    
    {
        name: "Mango",
        color: "Yellow",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Phillipines", "India"]
    }
]);

// Using the aggregate method
/*
	$match
		SYNTAX:
			{$match: {field: value}}

	$group
		SYNTAX:
			{$group: {_id:"value", fieldResult: "valueResult"}}

	SYNTAX:
		db.collectionName.aggregate([
			{ $match: {fieldA: value} },
			{ $group: {_id: "fieldB", fieldResult: "valueResult"} }
		])
*/

// Getting items on sale and the total stocks from suppliers
db.fruits.aggregate([
	{ $match: {onSale: true} },
	{ $group: {_id: "$supplier_id", total: {$sum: "$stock"}} }
]);

// Field projection with aggregation
/*
	$project
		SYNTAX:
			{ $project: { field: 1/0 } }
*/

db.fruits.aggregate([
    { $match: {onSale: true} },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $project: {_id: 0} }
]);

// Sorting 
/*
	$sort
		SYNTAX:
			{ $sort: { field: 1/-1 } }
*/
db.fruits.aggregate([
    { $match: {onSale: true} },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $project: {_id: 0} },
    { $sort: { total: 1 } }
]);

// Aggregating results based on array fields
/*
	$unwind
		SYNTAX:
			{ $unwind: field }
*/
db.fruits.aggregate([
    { $unwind: "$origin" }
]);

// Displays fruit documents by their origin and the kinds of fruits that are supplied

var owner = ObjectId();

db.owner.insert({
	_id: owner,
	name: "John Smith",
	contact: "0987654321"
});

/*
{
    "_id" : ObjectId("63e271d994a1c56ac3f88ad9"),
    "name" : "John Smith",
    "contact" : "0987654321"
}
*/

// Create supplier documents
db.suppliers.insert({
    name: "ABC Fruits",
    contact: "1234567890",
    owner_id: "63e271d994a1c56ac3f88ad9"
});

// One-To-One
db.suppliers.insert({
    name: "DEF Fruits",
    contact: "1234567890",
    addresses: [
        { street: "123 San JOse St", city: "Manila" },
        { street: "367 Gil Puyat", city: "Makati" }
    ]
});

// One-To-Many Relationships
var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.suppliers.insert({
    _id: supplier,
    name: "GHI Fruits",
    contact: "1234567890",
    branches: [
        branch1
    ]
});



